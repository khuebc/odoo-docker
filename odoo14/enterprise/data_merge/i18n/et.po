# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* data_merge
# 
# Translators:
# Martin Trigaux, 2020
# Arma Gedonsky <armagedonsky@hot.ee>, 2020
# Egon Raamat <egon@avalah.ee>, 2020
# Martin Aavastik <martin@avalah.ee>, 2020
# Helen Sulaoja <helen@avalah.ee>, 2020
# Algo Kärp <algokarp@gmail.com>, 2020
# Piia Paurson <piia@avalah.ee>, 2020
# Triine Aavik <triine@avalah.ee>, 2021
# Eneli Õigus <enelioigus@gmail.com>, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~13.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-01 07:39+0000\n"
"PO-Revision-Date: 2020-09-07 08:18+0000\n"
"Last-Translator: Eneli Õigus <enelioigus@gmail.com>, 2021\n"
"Language-Team: Estonian (https://www.transifex.com/odoo/teams/41243/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/js/data_merge_list_view.js:0
#, python-format
msgid "%s records have been merged"
msgstr "%s kirjet on ühendatud"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.notification
msgid ""
"' deduplication rule.<br/>\n"
"You can merge them"
msgstr ""
"' deduplikatsiooni reegel.<br/>\n"
"Sa võid need ühendada "

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid "<span class=\"mr-1\">Every</span>"
msgstr "<span class=\"mr-1\">Iga</span>"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid ""
"<span class=\"o_form_label\" attrs=\"{'invisible': [('custom_merge_method', "
"'=', False)]}\">Model specific</span>"
msgstr ""

#. module: data_merge
#: model:ir.model.constraint,message:data_merge.constraint_data_merge_rule_uniq_model_id_field_id
msgid "A field can only appear once!"
msgstr "Väli võib olla kasutuses ainult korra!"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__active
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__active
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__active
msgid "Active"
msgstr "Aktiivne"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__removal_mode__archive
msgid "Archive"
msgstr "Arhiveeri"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.data_merge_model_view_search
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_search
msgid "Archived"
msgstr "Arhiveeritud"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/js/data_merge_list_view.js:0
#, python-format
msgid ""
"Are you sure that you want to merge the selected records in their respective"
" group?"
msgstr ""
"Kas te olete kindel, et soovite ühendada valitud kirjed vastavate "
"gruppidega?"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/js/data_merge_list_view.js:0
#, python-format
msgid "Are you sure that you want to merge these records?"
msgstr "Kas olete kindel, et soovite neid kirjeid ühendada?"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__merge_mode__automatic
msgid "Automatic"
msgstr "Automaatne"

#. module: data_merge
#: code:addons/data_merge/models/data_merge_rule.py:0
#, python-format
msgid "Case/Accent Insensitive Match"
msgstr "Ei erista suuri ja väikeseid algustähti"

#. module: data_merge
#: model_terms:ir.actions.act_window,help:data_merge.action_data_merge_record
#: model_terms:ir.actions.act_window,help:data_merge.action_data_merge_record_notification
msgid "Configure rules to identify duplicate records"
msgstr "Seadista reeglid, et leida duplikaate"

#. module: data_merge
#: model:ir.model,name:data_merge.model_res_partner
msgid "Contact"
msgstr "Kontakt"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__record_create_uid
msgid "Created By"
msgstr "Loonud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__record_create_date
msgid "Created On"
msgstr "Loomise kuupäev"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__create_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__create_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__create_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__create_uid
msgid "Created by"
msgstr "Loonud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__create_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__create_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__create_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__create_date
msgid "Created on"
msgstr "Loodud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__mix_by_company
msgid "Cross-Company"
msgstr "Ettevõtete ülene"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__custom_merge_method
msgid "Custom Merge Method"
msgstr ""

#. module: data_merge
#: model:ir.actions.server,name:data_merge.ir_cron_cleanup_ir_actions_server
#: model:ir.cron,cron_name:data_merge.ir_cron_cleanup
#: model:ir.cron,name:data_merge.ir_cron_cleanup
msgid "Data Merge: Cleanup Records"
msgstr "Andmete ühendamine: puhasta andmeid"

#. module: data_merge
#: model:ir.actions.server,name:data_merge.ir_cron_find_duplicates_ir_actions_server
#: model:ir.cron,cron_name:data_merge.ir_cron_find_duplicates
#: model:ir.cron,name:data_merge.ir_cron_find_duplicates
msgid "Data Merge: Find Duplicate Records"
msgstr "Andmete ühendamine: leia duplikaatandmeid"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__notify_frequency_period__days
msgid "Days"
msgstr "päeva järel"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid "Deduplicate"
msgstr "Deduplikeeri"

#. module: data_merge
#: model:ir.ui.menu,name:data_merge.menu_data_merge_config_rules_deduplication
#: model:ir.ui.menu,name:data_merge.menu_data_merge_group
msgid "Deduplication"
msgstr "Deduplikatsioon"

#. module: data_merge
#: model:ir.model,name:data_merge.model_data_merge_group
msgid "Deduplication Group"
msgstr "Deduplikatsiooni grupp"

#. module: data_merge
#: model:ir.model,name:data_merge.model_data_merge_model
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__model_id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__model_id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__model_id
msgid "Deduplication Model"
msgstr "Deduplikatsiooni mudel"

#. module: data_merge
#: model:ir.model,name:data_merge.model_data_merge_record
msgid "Deduplication Record"
msgstr "Deduplikeeri andmed"

#. module: data_merge
#: model:ir.model,name:data_merge.model_data_merge_rule
msgid "Deduplication Rule"
msgstr "Deduplikatsiooni reegel"

#. module: data_merge
#: model:ir.actions.act_window,name:data_merge.action_data_merge_config
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__rule_ids
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid "Deduplication Rules"
msgstr "Deduplikatsiooni reeglid"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__removal_mode__delete
msgid "Delete"
msgstr "Kustuta"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__differences
msgid "Differences"
msgstr "Erinevused"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_record__differences
msgid "Differences with the master record"
msgstr "Erinevused põhiandmetega"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_list
msgid "Discard"
msgstr "Loobu"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_search
msgid "Discarded"
msgstr "Thistatud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__display_name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__display_name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__display_name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__display_name
#: model:ir.model.fields,field_description:data_merge.field_res_partner__display_name
msgid "Display Name"
msgstr "Kuva nimi"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__divergent_fields
msgid "Divergent Fields"
msgstr "Erienvad väljad"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__domain
msgid "Domain"
msgstr "Domeen"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__removal_mode
msgid "Duplicate Removal"
msgstr "Duplikaatide eemaldamine"

#. module: data_merge
#: model:ir.actions.act_window,name:data_merge.action_data_merge_record
#: model:ir.actions.act_window,name:data_merge.action_data_merge_record_notification
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid "Duplicates"
msgstr "Duplikaadid"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__create_threshold
msgid ""
"Duplicates with a similarity below this threshold will not be suggested"
msgstr "Duplikaate, millel on sarnasuse eesmärk alla selle, ei soovitata."

#. module: data_merge
#: code:addons/data_merge/models/data_merge_rule.py:0
#, python-format
msgid "Exact Match"
msgstr "Täpne vaste"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__field_values
msgid "Field Values"
msgstr "Välja väärtused"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.notification
msgid "I've identified"
msgstr "Olen tuvastanud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__id
#: model:ir.model.fields,field_description:data_merge.field_res_partner__id
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_list
msgid "ID"
msgstr "ID"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__is_deleted
msgid "Is Deleted"
msgstr "On kustutatud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__is_discarded
msgid "Is Discarded"
msgstr "On tühistatud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__is_master
msgid "Is Master"
msgstr "On põhiline"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group____last_update
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model____last_update
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record____last_update
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule____last_update
#: model:ir.model.fields,field_description:data_merge.field_res_partner____last_update
msgid "Last Modified on"
msgstr "Viimati muudetud (millal)"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__last_notification
msgid "Last Notification"
msgstr "Viimane teavitus"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__write_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__write_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__write_uid
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__write_uid
msgid "Last Updated by"
msgstr "Viimati uuendatud (kelle poolt)"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__write_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__write_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__write_date
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__write_date
msgid "Last Updated on"
msgstr "Viimati uuendatud"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_record__used_in
msgid "List of other models referencing this record"
msgstr "Mudelite nimekiri, mis viitavad sellele kirjele"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__notify_user_ids
msgid "List of users to notify when there are new records to merge"
msgstr ""
"Kasutajate nimekiri, keda tuleks teavitada, kui andmeid on vaja ühendada"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/xml/data_merge_list_views.xml:0
#, python-format
msgid "Main actions"
msgstr "Peamised tegevused"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__merge_mode__manual
msgid "Manual"
msgstr "Käsitsi"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/xml/data_merge_list_views.xml:0
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_list
#, python-format
msgid "Merge"
msgstr "Ühenda"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__match_mode
msgid "Merge If"
msgstr "Ühenda kui"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__merge_mode
msgid "Merge Mode"
msgstr "Ühendamise viis"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__res_model_id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__res_model_id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__res_model_id
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__res_model_id
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_search
msgid "Model"
msgstr "Mudel"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__res_model_name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__res_model_name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__res_model_name
msgid "Model Name"
msgstr "Mudeli nimetus"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__notify_frequency_period__months
msgid "Months"
msgstr "kuu järel"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__name
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__name
msgid "Name"
msgstr "Nimi"

#. module: data_merge
#: model_terms:ir.actions.act_window,help:data_merge.action_data_merge_record
#: model_terms:ir.actions.act_window,help:data_merge.action_data_merge_record_notification
msgid "No duplicates found"
msgstr "Duplikaate ei leitud."

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__notify_frequency
msgid "Notify"
msgstr "Teavita"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__notify_frequency_period
msgid "Notify Frequency Period"
msgstr "Teavitamise sagedus"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__notify_user_ids
msgid "Notify Users"
msgstr "Teavita kasutajaid"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__record_ids
msgid "Record"
msgstr "Record"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__group_id
msgid "Record Group"
msgstr "Kirje grupp"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__res_id
msgid "Record ID"
msgstr "Kirje ID"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_search
msgid "Records"
msgstr "Kirjet"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__records_to_merge_count
msgid "Records To Merge Count"
msgstr "Ühendamist ootavate andmete arv"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__domain
msgid "Records eligible for the deduplication process"
msgstr "Deduplikeerimise jaoks valmis andmed"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__merge_threshold
msgid ""
"Records with a similarity percentage above this threshold will be "
"automatically merged"
msgstr ""
"Kõik andmed, mille sarnasus ületab sarnasuse eesmärgi, ühendatakse "
"automaatselt."

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_record_search
msgid "Rule"
msgstr "Reegel"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.view_data_merge_model_form
msgid "Select a model to configure deduplication rules"
msgstr "Vali mudel, et kirjeldada deduplikatsiooni reegleid"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__sequence
msgid "Sequence"
msgstr "Järjestus"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_group__similarity
msgid "Similarity %"
msgstr "Sarnasuse %"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__merge_threshold
msgid "Similarity Threshold"
msgstr "Sarnasuse eesmärk"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_group__similarity
msgid ""
"Similarity coefficient based on the amount of text fields exactly in common."
msgstr ""
"Sarnasuse koefitsent tulenevalt tekstiväljadest, mis on täpselt samasugused."

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__rule_ids
msgid "Suggest to merge records matching at least one of these rules"
msgstr "Soovita ühendada need andmed, kus vähemalt üks reeglitest klapib"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_model__create_threshold
msgid "Suggestion Threshold"
msgstr "Soovitatud eesmärk"

#. module: data_merge
#: model:ir.model.constraint,message:data_merge.constraint_data_merge_model_check_notif_freq
msgid "The notification frequency should be greater than 0"
msgstr "Teavituste sagedus peaks olema suurem kui 0"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/js/data_merge_list_view.js:0
#, python-format
msgid "The selected"
msgstr "Valitud"

#. module: data_merge
#: model:ir.model.constraint,message:data_merge.constraint_data_merge_model_uniq_name
msgid "This name is already taken"
msgstr "See nimi on juba võetud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_rule__field_id
msgid "Unique ID Field"
msgstr "Välja unikaalne ID"

#. module: data_merge
#. openerp-web
#: code:addons/data_merge/static/src/xml/data_merge_list_views.xml:0
#, python-format
msgid "Unselect"
msgstr "Eemalda valitud"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__record_write_uid
msgid "Updated By"
msgstr "Uuendatud (kelle poolt)"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__record_write_date
msgid "Updated On"
msgstr "Uuendatud (millal)"

#. module: data_merge
#: model:ir.model.fields,field_description:data_merge.field_data_merge_record__used_in
msgid "Used In"
msgstr "Kasutatakse"

#. module: data_merge
#: model:ir.model.fields.selection,name:data_merge.selection__data_merge_model__notify_frequency_period__weeks
msgid "Weeks"
msgstr "nädala järel"

#. module: data_merge
#: model:ir.model.fields,help:data_merge.field_data_merge_model__mix_by_company
msgid "When enabled, duplicates across different companies will be suggested"
msgstr "Kui lubatud, siis duplikaate soovitatakse kõigi ettevõtete üleselt."

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.notification
msgid "duplicate records with the '"
msgstr ""

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.notification
msgid "here"
msgstr "siin"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.merge_message
msgid "merged into"
msgstr "ühendati kokku"

#. module: data_merge
#: model_terms:ir.ui.view,arch_db:data_merge.merge_message_master
msgid "merged into this one"
msgstr "ühendati kokku järgmistega"
