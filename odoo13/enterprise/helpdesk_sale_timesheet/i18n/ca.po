# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* helpdesk_sale_timesheet
# 
# Translators:
# Manel Fernandez Ramirez <manelfera@outlook.com>, 2020
# Arnau Ros, 2020
# jabelchi, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-01 12:16+0000\n"
"PO-Revision-Date: 2019-08-26 09:36+0000\n"
"Last-Translator: jabelchi, 2021\n"
"Language-Team: Catalan (https://www.transifex.com/odoo/teams/41243/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: helpdesk_sale_timesheet
#: model:ir.model,name:helpdesk_sale_timesheet.model_account_analytic_line
msgid "Analytic Line"
msgstr "Línia analítica"

#. module: helpdesk_sale_timesheet
#: model:ir.model,name:helpdesk_sale_timesheet.model_helpdesk_ticket
msgid "Helpdesk Ticket"
msgstr "Tiquet de Helpdesk"

#. module: helpdesk_sale_timesheet
#: model:ir.model.fields,help:helpdesk_sale_timesheet.field_helpdesk_ticket__is_overdue
msgid ""
"Indicates more support time has been delivered thant the ordered quantity"
msgstr "Indica que s'ha entregat més temps de suport de la quantitat demanada"

#. module: helpdesk_sale_timesheet
#: model:ir.model.fields,field_description:helpdesk_sale_timesheet.field_helpdesk_ticket__is_overdue
msgid "Is overdue"
msgstr "Ha vençut"

#. module: helpdesk_sale_timesheet
#: model:ir.model.fields,help:helpdesk_sale_timesheet.field_helpdesk_ticket__use_helpdesk_sale_timesheet
msgid "Reinvoice the time spent on ticket through tasks."
msgstr "Refacturar el temps invertit en un tiquet per mitjà de tasques."

#. module: helpdesk_sale_timesheet
#: model:ir.model.fields,field_description:helpdesk_sale_timesheet.field_helpdesk_ticket__use_helpdesk_sale_timesheet
msgid "Reinvoicing Timesheet activated on Team"
msgstr "Refacturar fulls d'hores activades a l'equip"
