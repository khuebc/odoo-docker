# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* approvals
# 
# Translators:
# Martin Trigaux, 2019
# Jaroslav Helemik Nemec <nemec@helemik.cz>, 2019
# Jakub Lohnisky <jakub@lepremier.cz>, 2019
# Ladislav Tomm <tomm@helemik.cz>, 2019
# Jan Horzinka <jan.horzinka@centrum.cz>, 2019
# Michal Veselý <michal@veselyberanek.net>, 2019
# Rastislav Brencic <rastislav.brencic@azet.sk>, 2020
# trendspotter, 2020
# karolína schusterová <karolina.schusterova@vdp.sk>, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-05 09:37+0000\n"
"PO-Revision-Date: 2019-08-26 09:35+0000\n"
"Last-Translator: karolína schusterová <karolina.schusterova@vdp.sk>, 2021\n"
"Language-Team: Czech (https://www.transifex.com/odoo/teams/41243/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "<span>From: </span>"
msgstr "<span>Od: </span>"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "<span>to: </span>"
msgstr "<span>komu: </span>"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_needaction
msgid "Action Needed"
msgstr "Vyžaduje akci"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__active
msgid "Active"
msgstr "Aktivní"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_ids
msgid "Activities"
msgstr "Aktivity"

#. module: approvals
#: model:ir.model,name:approvals.model_mail_activity
msgid "Activity"
msgstr "Činnost"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_exception_decoration
msgid "Activity Exception Decoration"
msgstr "Dekorace výjimky aktivity"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_state
msgid "Activity State"
msgstr "Stav aktivity"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_category__has_item
#: model:ir.model.fields,help:approvals.field_approval_request__has_item
msgid "Additional items that should be specified on the request."
msgstr ""

#. module: approvals
#: model:res.groups,name:approvals.group_approval_manager
msgid "Administrator"
msgstr "Administrátor"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_request_action_all
#: model:ir.ui.menu,name:approvals.approvals_approval_menu_all
msgid "All Approvals"
msgstr "Všechna schválení"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__amount
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Amount"
msgstr "Částka"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_category__has_reference
#: model:ir.model.fields,help:approvals.field_approval_request__has_reference
msgid "An additional reference that should be specified on the request."
msgstr "Další odkaz, který by měl být uveden v žádosti."

#. module: approvals
#: model:mail.activity.type,name:approvals.mail_activity_data_approval
msgid "Approval"
msgstr "Schválení"

#. module: approvals
#: model:ir.model,name:approvals.model_approval_category
msgid "Approval Category"
msgstr "Kategorie schválení"

#. module: approvals
#: model:ir.model,name:approvals.model_approval_request
msgid "Approval Request"
msgstr "Žádost o schválení"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__name
msgid "Approval Subject"
msgstr "Předmět schválení"

#. module: approvals
#: model:ir.ui.menu,name:approvals.approvals_menu_root
msgid "Approvals"
msgstr "Schválení"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Approvals Type Name"
msgstr "Název typu schválení"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_category_action
#: model:ir.ui.menu,name:approvals.approvals_category_menu_config
msgid "Approvals Types"
msgstr "Typy schválení"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_kanban
msgid "Approvals Types Image"
msgstr "Obrázek typů schválení"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_request_action_to_review
#: model:ir.ui.menu,name:approvals.approvals_approval_menu_to_review
msgid "Approvals to Review"
msgstr "Schválení ke kontrole"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_request_action_to_review_category
msgid "Approvals to review"
msgstr "Schválení ke kontrole"

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/activity.xml:0
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
#, python-format
msgid "Approve"
msgstr "Schválit"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_approver__status__approved
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__request_status__approved
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__user_status__approved
msgid "Approved"
msgstr "Scháleno"

#. module: approvals
#: model:ir.model,name:approvals.model_approval_approver
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
#: model:res.groups,name:approvals.group_approval_user
msgid "Approver"
msgstr "Schvalovatel"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Approver(s)"
msgstr "Schvalovatel"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__user_ids
#: model:ir.model.fields,field_description:approvals.field_approval_request__approver_ids
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Approvers"
msgstr "Schvalovatelé"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Archived"
msgstr "Archivováno"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Attach Document"
msgstr "Připojit dokument"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_attachment_count
msgid "Attachment Count"
msgstr "Počet příloh"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_category__is_manager_approver
#: model:ir.model.fields,help:approvals.field_approval_request__is_manager_approver
msgid "Automatically add the manager as approver on the request."
msgstr "Automaticky přidat správce jako schvalovače na žádost."

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Back To Draft"
msgstr "Zpět na koncept"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_borrow_items
msgid "Borrow Items"
msgstr "Půjčte si předměty"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_business_trip
msgid "Business Trip"
msgstr "Obchodní cesta"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_approver__status__cancel
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__request_status__cancel
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__user_status__cancel
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Cancel"
msgstr "Zrušit"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_car_rental_application
msgid "Car Rental Application"
msgstr "Aplikace pro pronájem automobilů"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__category_id
msgid "Category"
msgstr "Kategorie"

#. module: approvals
#: model:ir.ui.menu,name:approvals.approvals_menu_config
msgid "Configuration"
msgstr "Konfigurace"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__partner_id
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Contact"
msgstr "Kontakt"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_contract_approval
msgid "Contract Approval"
msgstr "Schválení smlouvy"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__create_uid
#: model:ir.model.fields,field_description:approvals.field_approval_category__create_uid
#: model:ir.model.fields,field_description:approvals.field_approval_request__create_uid
msgid "Created by"
msgstr "Vytvořeno od"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__create_date
#: model:ir.model.fields,field_description:approvals.field_approval_category__create_date
#: model:ir.model.fields,field_description:approvals.field_approval_request__create_date
msgid "Created on"
msgstr "Vytvořeno"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_category_action_new_request
msgid "Dashboard"
msgstr "Ovládací panel"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__date
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Date"
msgstr "Datum"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__date_confirmed
msgid "Date Confirmed"
msgstr "Datum potvrzeno"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__date_end
msgid "Date end"
msgstr "Konec data"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__date_start
msgid "Date start"
msgstr "Datum zahájení"

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#, python-format
msgid "Deadline"
msgstr "Uzávěrka"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
msgid "Delete"
msgstr "Smazat"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__description
#: model:ir.model.fields,field_description:approvals.field_approval_request__reason
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Description"
msgstr "Popis"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__display_name
#: model:ir.model.fields,field_description:approvals.field_approval_category__display_name
#: model:ir.model.fields,field_description:approvals.field_approval_request__display_name
msgid "Display Name"
msgstr "Zobrazované jméno"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Document"
msgstr "Dokument"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__requirer_document
#: model:ir.model.fields,field_description:approvals.field_approval_request__requirer_document
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Documents"
msgstr "Dokumenty"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
msgid "Dropdown menu"
msgstr "Rozbalovací nabídka"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "E.g: Expenses Paris business trip"
msgstr "Např .: Výdaje na služební cestu v Paříži"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
msgid "Edit Request"
msgstr "Žádost o úpravu"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__is_manager_approver
#: model:ir.model.fields,field_description:approvals.field_approval_request__is_manager_approver
msgid "Employee's Manager"
msgstr "Zaměstnanecký manažer"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Fields"
msgstr "Pole"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_follower_ids
msgid "Followers"
msgstr "Sledující"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_channel_ids
msgid "Followers (Channels)"
msgstr "Sledující (kanály)"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_partner_ids
msgid "Followers (Partners)"
msgstr "Sledující (partneři)"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_general_approval
msgid "General Approval"
msgstr "Obecné schválení"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_access_to_request
msgid "Has Access To Request"
msgstr "Má přístup k požadavku"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_amount
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_amount
msgid "Has Amount"
msgstr "Má částku"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_partner
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_partner
msgid "Has Contact"
msgstr "Má kontakt"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_date
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_date
msgid "Has Date"
msgstr "Má datum"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_item
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_item
msgid "Has Item"
msgstr "Obsahuje položku"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_location
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_location
msgid "Has Location"
msgstr "Má polohu"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_payment_method
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_payment_method
msgid "Has Payment"
msgstr "Má platbu"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_period
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_period
msgid "Has Period"
msgstr "Má období"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_quantity
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_quantity
msgid "Has Quantity"
msgstr "Má množství"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__has_reference
#: model:ir.model.fields,field_description:approvals.field_approval_request__has_reference
msgid "Has Reference"
msgstr "Má odkaz"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__id
#: model:ir.model.fields,field_description:approvals.field_approval_category__id
#: model:ir.model.fields,field_description:approvals.field_approval_request__id
msgid "ID"
msgstr "ID"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_exception_icon
msgid "Icon"
msgstr "Ikona"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__activity_exception_icon
msgid "Icon to indicate an exception activity."
msgstr "Ikona označuje vyjímečnou aktivitu."

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__message_needaction
#: model:ir.model.fields,help:approvals.field_approval_request__message_unread
msgid "If checked, new messages require your attention."
msgstr "Pokud je zaškrtnuto, nové zprávy vyžadují vaši pozornost."

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__message_has_error
#: model:ir.model.fields,help:approvals.field_approval_request__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "Pokud je zaškrtnuto, některé zprávy mají chybu při doručení."

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__image
msgid "Image"
msgstr "Obrázek"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_is_follower
msgid "Is Follower"
msgstr "Je sledující"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__items
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Items"
msgstr "Položky"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_job_referral_award
msgid "Job Referral Award"
msgstr "Ocenění za doporučení práce"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver____last_update
#: model:ir.model.fields,field_description:approvals.field_approval_category____last_update
#: model:ir.model.fields,field_description:approvals.field_approval_request____last_update
msgid "Last Modified on"
msgstr "Naposled změněno"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__write_uid
#: model:ir.model.fields,field_description:approvals.field_approval_category__write_uid
#: model:ir.model.fields,field_description:approvals.field_approval_request__write_uid
msgid "Last Updated by"
msgstr "Naposledy upraveno od"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__write_date
#: model:ir.model.fields,field_description:approvals.field_approval_category__write_date
#: model:ir.model.fields,field_description:approvals.field_approval_request__write_date
msgid "Last Updated on"
msgstr "Naposled upraveno"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__location
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Location"
msgstr "Místo"

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#, python-format
msgid "Log"
msgstr "Záznam"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_main_attachment_id
msgid "Main Attachment"
msgstr "Hlavní příloha"

#. module: approvals
#: model:ir.ui.menu,name:approvals.approvals_menu_manager
msgid "Manager"
msgstr "Manažer"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_has_error
msgid "Message Delivery error"
msgstr "Chyba při doručování zpráv"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_ids
msgid "Messages"
msgstr "Zprávy"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__approval_minimum
#: model:ir.model.fields,field_description:approvals.field_approval_request__approval_minimum
msgid "Minimum Approval"
msgstr "Minimální schválení"

#. module: approvals
#: model:ir.ui.menu,name:approvals.approvals_approval_menu
msgid "My Approvals"
msgstr "Moje schválení"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_search_view_search
msgid "My Approvals to Review"
msgstr "Moje schválení ke kontrole"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_search_view_search
msgid "My Request"
msgstr "Moje žádost"

#. module: approvals
#: model:ir.actions.act_window,name:approvals.approval_request_action
#: model:ir.ui.menu,name:approvals.approvals_request_menu_my
msgid "My Requests"
msgstr "Mé žádosti"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__name
msgid "Name"
msgstr "Název"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_approver__status__new
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__user_status__new
msgid "New"
msgstr "Nové"

#. module: approvals
#: model:ir.ui.menu,name:approvals.approvals_category_menu_new
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_kanban
msgid "New Request"
msgstr "Nový požadavek"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_date_deadline
msgid "Next Activity Deadline"
msgstr "Termín další aktivity"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_summary
msgid "Next Activity Summary"
msgstr "Souhrn další aktivity"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_type_id
msgid "Next Activity Type"
msgstr "Další typ aktivity"

#. module: approvals
#: model_terms:ir.actions.act_window,help:approvals.approval_request_action_to_review
msgid "No new approvals to review"
msgstr "Žádná nová schválení ke kontrole"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_amount__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_date__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_item__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_location__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_partner__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_payment_method__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_period__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_quantity__no
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_reference__no
msgid "None"
msgstr "Nic"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_needaction_counter
msgid "Number of Actions"
msgstr "Počet akcí"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__attachment_number
msgid "Number of Attachments"
msgstr "Počet příloh"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_has_error_counter
msgid "Number of errors"
msgstr "Počet chyb"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Počet zpráv, které vyžadují akci"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Počet zpráv s chybou při doručení"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__request_to_validate_count
msgid "Number of requests to validate"
msgstr "Počet žádostí o ověření"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__message_unread_counter
msgid "Number of unread messages"
msgstr "Počet nepřečtených zpráv"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_amount__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_date__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_item__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_location__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_partner__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_payment_method__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_period__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_quantity__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_reference__optional
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__requirer_document__optional
msgid "Optional"
msgstr "Volitelné"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Options"
msgstr "Možnosti"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Payment"
msgstr "Platba"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_payment_application
msgid "Payment Application"
msgstr "Žádost o platbu"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Period"
msgstr "Období"

#. module: approvals
#: model:approval.category,name:approvals.approval_category_data_procurement
msgid "Procurement"
msgstr "Zásobování"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__quantity
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Quantity"
msgstr "Množství"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__reference
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_form
msgid "Reference"
msgstr "Reference"

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/activity.xml:0
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
#, python-format
msgid "Refuse"
msgstr "Odmítnout"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_approver__status__refused
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__request_status__refused
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__user_status__refused
msgid "Refused"
msgstr "Odmítnuto"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__request_id
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Request"
msgstr "Požadavek"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.res_users_view_form
msgid "Request Approval"
msgstr "Požádat o schválení"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__request_owner_id
msgid "Request Owner"
msgstr "Požádat vlastníka"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__request_status
msgid "Request Status"
msgstr "Stav požadavku"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_amount__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_date__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_item__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_location__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_partner__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_payment_method__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_period__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_quantity__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__has_reference__required
#: model:ir.model.fields.selection,name:approvals.selection__approval_category__requirer_document__required
msgid "Required"
msgstr "Povinné"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__activity_user_id
msgid "Responsible User"
msgstr "Zodpovědný uživatel"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_has_sms_error
msgid "SMS Delivery error"
msgstr "Chyba doručení SMS"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_category__sequence
msgid "Sequence"
msgstr "Číselná řada"

#. module: approvals
#: code:addons/approvals/models/approval_request.py:0
#, python-format
msgid "State change."
msgstr "Změna stavu."

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_approver__status
msgid "Status"
msgstr "Stav"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__activity_state
msgid ""
"Status based on activities\n"
"Overdue: Due date is already passed\n"
"Today: Activity date is today\n"
"Planned: Future activities."
msgstr ""
"Stav na základě aktivit\n"
"Vypršeno: Datum již uplynulo\n"
"Dnes: Datum aktivity je dnes\n"
"Plánováno: Budoucí aktivity."

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
msgid "Submit"
msgstr "Odeslat"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__request_status__pending
msgid "Submitted"
msgstr "Odesláno"

#. module: approvals
#: model:res.groups,comment:approvals.group_approval_user
msgid "The user will be able to see approvals created by himself."
msgstr "Uživatel bude moci zobrazit schválení vytvořená sám."

#. module: approvals
#: model:res.groups,comment:approvals.group_approval_manager
msgid "The user will have access to the approvals configuration."
msgstr "Uživatel bude mít přístup ke konfiguraci schválení."

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/activity.xml:0
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#: model:ir.model.fields.selection,name:approvals.selection__approval_approver__status__pending
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__user_status__pending
#, python-format
msgid "To Approve"
msgstr "Ke schválení"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_category_view_kanban
msgid "To Review:"
msgstr "Ke kontrole:"

#. module: approvals
#: model:ir.model.fields.selection,name:approvals.selection__approval_request__request_status__new
msgid "To Submit"
msgstr "Předložit"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__activity_exception_decoration
msgid "Type of the exception activity on record."
msgstr "Typ zaznamenané výjimečné aktivity."

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_unread
msgid "Unread Messages"
msgstr "Nepřečtené zprávy"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__message_unread_counter
msgid "Unread Messages Counter"
msgstr "Počítadlo nepřečtených zpráv"

#. module: approvals
#. openerp-web
#: code:addons/approvals/static/src/xml/web_kanban_activity.xml:0
#: model:ir.model.fields,field_description:approvals.field_approval_approver__user_id
#, python-format
msgid "User"
msgstr "Uživatel"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__user_status
msgid "User Status"
msgstr "Stav uživatele"

#. module: approvals
#: model:ir.model.fields,field_description:approvals.field_approval_request__website_message_ids
msgid "Website Messages"
msgstr "Zprávy webové stránky"

#. module: approvals
#: model:ir.model.fields,help:approvals.field_approval_request__website_message_ids
msgid "Website communication history"
msgstr "Historie komunikace webové stránky"

#. module: approvals
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_form
#: model_terms:ir.ui.view,arch_db:approvals.approval_request_view_kanban
msgid "Withdraw"
msgstr "Ustoupit"

#. module: approvals
#: code:addons/approvals/models/approval_request.py:0
#, python-format
msgid "You have to add at least %s approvers to confirm your request."
msgstr "Musíte přidat alespoň %s schvalovatelé k potvrzení vaší žádosti."

#. module: approvals
#: code:addons/approvals/models/approval_request.py:0
#, python-format
msgid "You have to attach at lease one document."
msgstr "Při pronájmu musíte připojit jeden dokument."

#. module: approvals
#: code:addons/approvals/models/approval_request.py:0
#, python-format
msgid "Your request %s is now in the state %s"
msgstr ""
